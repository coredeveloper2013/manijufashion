<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class VendorImage extends Model
{
    protected $fillable = [
        'type', 'image_path', 'status', 'sort', 'url', 'color'
    ];
}
