<?php

namespace App\Http\Controllers;

use App\Enumeration\PageEnumeration;
use App\Enumeration\Role;
use App\Model\Category;
use App\Model\Item;
use App\Model\MasterColor;
use App\Model\MetaVendor;
use App\Model\TopBanner;
use App\Model\WishListItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NewArrivalController extends Controller
{
    public function showItems(Request $request) {
        $date = '';
        $cat = '';

        $query = Item::query();
        if ( Auth::guest() ) {
            $query->where('status', 1)->where('guest_image', 1);
        }
        else {
            $query->where('status', 1)->where('guest_image', 0);
        }

        if (isset($request->D)) {
            if ($request->D != 'A') {
                $date = date('Y-m-d', strtotime("-$request->D day"));
                $query->whereDate('created_at', $date);
            }
        }

        if (isset($request->C)) {
            $cat = $request->C;
            $query->where('default_parent_category', $request->C);
        }

        $query->orderBy('created_at', 'desc');
        //$items = $query->paginate(30);
        $items = [];

        // Category
        $categories = Category::where('parent', 0)
            ->orderBy('sort')
            ->get();

        foreach ($categories as &$category) {
            $categoryCountQuery = Item::query();
            $categoryCountQuery->where('status', 1)->where('default_parent_category', $category->id);

            if ($date == '') {
                $category->count = $categoryCountQuery->count();
            } else {
                $category->count = $categoryCountQuery->whereDate('created_at', $date)->count();
            }
        }

        // Arrival Dates
        $totalCount = Item::where('status', 1)->count();
        $byArrivalDate = [];
        $byArrivalDate[] = [
            'name' => 'All',
            'count' => number_format($totalCount),
            'day' => 'A'
        ];

        for($i=0; $i <= 6; $i++) {
            $count = Item::where('status', 1)
                ->whereDate('created_at', date('Y-m-d', strtotime("-$i day")))
                ->count();

            $byArrivalDate[] = [
                'name' => date('F j', strtotime("-$i day")),
                'count' => number_format($count),
                'day' => $i
            ];
        }

        // Vendors
        $vendors = MetaVendor::where('verified', 1)
            ->where('active', 1)
            ->orderBy('company_name')->get();

        // Wishlist
        $obj = new WishListItem();
        $wishListItems = $obj->getItemIds();

        // Master Colors
        $masterColors = MasterColor::orderBy('name')->get();

        $defaultItemImage = DB::table('settings')->where('name', 'default-item-image')->first();
        if ($defaultItemImage)
            $defaultItemImage_path = asset($defaultItemImage->value);

        return view('pages.new_arrival', compact('items', 'byArrivalDate', 'categories', 'vendors',
            'masterColors', 'wishListItems', 'defaultItemImage_path'));
    }

    public function getNewArrivalItems(Request $request) {
        $query = Item::query();

        $query->where('status', 1);

        if ($request->D && $request->D != '') {
            if ($request->D != 'A') {
                $date = date('Y-m-d', strtotime("-$request->D day"));
                $query->whereDate('created_at', $date);
            }
        }

        if ($request->categories && sizeof($request->categories) > 0)
            $query->whereIn('default_second_category', $request->categories);

        if ($request->vendors && sizeof($request->vendors) > 0)
            $query->whereIn('vendor_meta_id', $request->vendors);


        if ($request->masterColors && sizeof($request->masterColors) > 0) {
            $masterColors = $request->masterColors;

            $query->whereHas('colors', function ($query) use ($masterColors) {
                $query->whereIn('master_color_id', $masterColors);
            });
        }

        // Search
        if ($request->searchText && $request->searchText != ''){
            if ($request->searchOption == '1')
                $query->where('style_no', 'like','%'.$request->searchText.'%');
            if ($request->searchOption == '2')
                $query->where('description', 'like','%'.$request->searchText.'%');
        }

        if ($request->priceMin && $request->priceMin != '')
            $query->where('price', '>=',$request->priceMin);

        if ($request->priceMax && $request->priceMax != '')
            $query->where('price', '<=',$request->priceMax);

        $query->where('activated_at', '>=', date('Y-m-d', strtotime('-30 days')));

        // Sorting
        $query->orderBy('activated_at', 'desc');
        
        if ($request->sorting){
            if ($request->sorting == '1') {
                $query->orderBy('sorting');
                $query->orderBy('activated_at', 'desc');
            } else if ($request->sorting == '2')
                $query->orderBy('price');
            else if ($request->sorting == '3')
                $query->orderBy('price', 'desc');
            else if ($request->sorting == '4')
                $query->orderBy('style_no');
        }

        $items = $query->with('images', 'vendor', 'colors')->paginate(52);
        $paginationView = $items->links('others.pagination');
        $paginationView = trim(preg_replace('/\r\n/', ' ', $paginationView));

        // Blocked check
        $blockedVendorIds = [];

        foreach($items as &$item) {
            // Price
            $price = '';
            $colorsImages = [];

            foreach($item->colors as $color) {
                foreach ($item->images as $image) {
                    if ($image->color_id == $color->id) {
                        $colorsImages[$color->name] = asset($image->thumbs_image_path);
                        break;
                    }
                }
            }

            if (Auth::check() && Auth::user()->role == Role::$BUYER) {
                if ($item->orig_price != null)
                    $price .= '<del>$' . number_format($item->orig_price, 2, '.', '') . '</del> ';

                $price .= '$' . number_format($item->price, 2, '.', '');
            }


            // Image
            $imagePath2 = '';
            $imagePath = asset('images/no-image.png');

            if (sizeof($item->images) > 0)
                $imagePath = asset($item->images[0]->list_image_path);

            if (sizeof($item->images) > 1)
                $imagePath2 = asset($item->images[1]->list_image_path);

            $item->imagePath = $imagePath;
            $item->imagePath2 = $imagePath2;
            $item->detailsUrl = route('item_details_page', ['item' => $item->id, 'name' => changeSpecialChar($item->name)]);
            $item->price = $price;
            $item->colorsImages = $colorsImages;
            $item->video = ($item->video) ? asset($item->video) : null;

            // Blocked Check
            if (in_array($item->vendor_meta_id, $blockedVendorIds)) {
                $item->imagePath = asset('images/blocked.jpg');
                $item->vendor->company_name = '';
                $item->vendorUrl = '';
                $item->style_no = '';
                $item->price = '';
                $item->available_on = '';
                $item->colors->splice(0);
            }
        }

        return ['items' => $items->toArray(), 'pagination' => $paginationView];
    }
}
