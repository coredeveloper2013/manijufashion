<footer class="footer_area">
    <div class="Footer_top show_desktop">
    {{-- <div class="Footer_top"> --}}
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="footer_menu">
                        <h2>COMPANY</h2>
                        <ul>
                            <li><a href="{{ route('about_us') }}">ABOUT US</a></li>
                            <li><a href="{{ route('contact_us') }}">CONTACT US</a></li>
                            <li><a href="{{ route('privacy_policy') }}">PRIVACY POLICY</a></li>
                            <li><a href="{{ route('terms_conditions') }}">TERMS & CONDITIONS</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer_menu">
                        <h2>SUPPORT</h2>
                        <ul>
                            <li><a href="{{ route('buyer_show_overview') }}">YOUR ACCOUNT</a></li>
                            <li><a href="{{ route('cookies_policy') }}">COOKIES POLICY</a></li>
                            <li><a href="{{ route('return_info') }}">RETURN INFO</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer_menu">
                        <h2>BUSINESS HOUR</h2>
                        <ul>
                            <li>Mon - Fri: 8AM - 6PM</li>
                            <li>Sat: Closed</li>
                            <li>Sun: Closed</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="footer_menu">
                        {{--<h2>POLICIES</h2>
                        <ul>
                            <li><a href="#">PRIVACY POLICY</a></li>
                            <li><a href="#">TERMS OF USE</a></li>
                            <li><a href="#">GIFT CARD CONDITIONS</a></li>
                            <li><a href="#">CALIFORNIA SUPPLY CHAINS ACT</a></li>
                        </ul>--}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="Footer_top show_mobile">
        <div class="footer_menu_list">
            <div id="accordion">
                <div class="card">
                    <div class="card-header" id="heading1">
                        <button class="btn-link collapsed" data-toggle="collapse" data-target="#collapseOne">
                            <p>COMPANY</p>
                        </button>
                    </div>
                    <div id="collapseOne" class="collapse">
                        <div class="card-body clearfix">
                            <ul>
                                <li><a href="{{ route('about_us') }}">ABOUT US</a></li>
                                <li><a href="{{ route('contact_us') }}">CONTACT US</a></li>
                                <li><a href="{{ route('privacy_policy') }}">PRIVACY POLICY</a></li>
                                <li><a href="{{ route('terms_conditions') }}">TERMS & CONDITIONS</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="heading2">
                        <button class="btn-link collapsed" data-toggle="collapse" data-target="#collapse2" >
                            <p>SUPPORT</p>
                        </button>
                    </div>
                    <div id="collapse2" class="collapse ">
                        <div class="card-body clearfix">
                            <ul>
                                <li><a href="{{ route('buyer_show_overview') }}">YOUR ACCOUNT</a></li>
                                <li><a href="{{ route('cookies_policy') }}">COOKIES POLICY</a></li>
                                <li><a href="{{ route('return_info') }}">RETURN INFO</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header" id="heading3">
                        <button class="btn-link collapsed" data-toggle="collapse" data-target="#collapse3" >
                            <p>BUSINESS HOUR</p>
                        </button>
                    </div>
                    <div id="collapse3" class="collapse ">
                        <div class="card-body clearfix">
                            <ul>
                                <li>Mon - Fri: 8AM - 6PM</li>
                                <li>Sat: Closed</li>
                                <li>Sun: Closed</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="footer_bottom">
        <div class="container-fluid">
            <div class="row">
                <div class="col-6 col-md-6 no-padding-left">
                    <div class="sitemap">
                        <h2>UNITED STATES</h2>
                        <ul>
                            @yield('breadcrumbs')
                        </ul>
                    </div>
                </div>
                <div class="col-6 col-md-6 no-padding-right">
                    <div class="footer_copyright text-right">
                        <ul>
                            <li>ENGLISH</li>
                            <li><a href="#">ESPAÑOL</a></li>
                        </ul>
                        <p>© All rights reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>