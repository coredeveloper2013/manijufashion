<ul>
    <li class="active"><a href="{{ route('buyer_show_overview') }}">My Profile</a></li>
    <li><a href="{{ route('buyer_my_information') }}">My Information</a></li>
    <li><a href="{{ route('buyer_billing') }}">Credit Cards and Billing</a></li>
    <li><a href="{{ route('buyer_show_orders') }}">Order History</a></li>
    <li><a href="{{ route('view_wishlist') }}">My Wishlist</a></li>
    <li><form action="{{ route('logout_buyer') }}" method="POST">@csrf<input type="submit" value="Sign Out"></form></li>
    {{--<li><a href="{{ route('buyer_beauty_bio') }}">My Beauty Bio</a></li>
    <li><a href="{{ route('buyer_gift_card') }}">Check Gift Card Balance</a></li>--}}
</ul>