<?php use App\Enumeration\OrderStatus; ?>
@extends('layouts.home_layout')

@section('content')
<!-- =========================
    START BANNER SECTION
============================== -->
<section class="banner_area common_banner clearfix">
    <div class="container container_full_width_mobile">
        <div class="row">
            <div class="col-md-12 custom_padding_9">
                <div class="banner_top">
                    <p>IT’S HERE: THE WEDDING SALE! | <a href="#">SHOP NEW MARKDOWNS</a> </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
    END BANNER SECTION
============================== -->  

<!-- =========================
    START APPOINMENT SECTION
============================== -->
<section class="appoinment_area common_content_area">
    <div class="container">
        <div class="row">
            <div class="col-md-2 custom_padding_9 for_desktop">
                <div class="common_left_menu">
                    @include('buyer.profile.menu')
                </div>
            </div>
            <div class="col-md-10">
                <div class="my_account_content my_information_content">
                    <div class="myaccount_title">
                        <h2>My Information</h2>
                    </div>
                    <div class="my_info_area">
                        <h2>Contact Information <a href="{{route('buyer_edit_shipping_info')}}">Edit</a></h2>
                        @if(auth()->user())
                        <p>{{auth()->user()->first_name}} &nbsp; {{auth()->user()->last_name}}</p>
                        <p>{{auth()->user()->email}}</p>
                        @endif
                        <p><a href="{{route('new_password_buyer')}}">Change Password</a></p>
                        <p><a href="{{route('buyer_get_change_avatar')}}">Upload Avatar</a></p>

                        <h2>Shipping Information <a href="{{route('buyer_edit_shipping_info')}}">Edit</a></h2>
                        <p>{{$buyerInfo->company_name}}</p>
                        <p>{{$buyerInfo->billing_location}}</p>
                        <address>
                            <p>{{(isset($buyerInfo->address1) ? $buyerInfo->address1 : 'Nan')}}</p>
                            <p>{{(isset($buyerInfo->address2) ? $buyerInfo->address2 : 'Nan')}}</p>
                            <p>{{($buyerInfo->city) ? $buyerInfo->city : 'Nan'}} , {{(isset($buyerInfo->state)) ? $buyerInfo->state : 'Nan'}}, {{(isset($buyerInfo->zip))? $buyerInfo->zip : 'Nan' }}</p>
                        </address>
                        <p>{{(isset($buyerInfo->country)) ? $buyerInfo->country : 'Nan'}}</p>
                        <p>T:{{ (isset($buyerInfo->mobile)) ? $buyerInfo->mobile : 'Nan' }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
    END APPOINMENT SECTION
============================== -->
@stop

@section('additionalJS')
    <script>
        $(function () {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': '{!! csrf_token() !!}'
                }
            });

            $('#btnApprove').click(function (e) {
                e.preventDefault();
                var id = $(this).data('id');

                $.ajax({
                    method: "POST",
                    url: "{{ route('order_reject_status_change') }}",
                    data: { id: id, status: 2 },
                }).done(function( data ) {
                    window.location.reload(true);
                });
            });

            $('#btnDecline').click(function (e) {
                e.preventDefault();
                var id = $(this).data('id');

                $.ajax({
                    method: "POST",
                    url: "{{ route('order_reject_status_change') }}",
                    data: { id: id, status: 1 },
                }).done(function( data ) {
                    window.location.reload(true);
                });
            });
        });
    </script>
@stop