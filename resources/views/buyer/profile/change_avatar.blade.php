@extends('layouts.home_layout')
@section('additionalCSS')
    <link href="{{ asset('plugins/toastr/toastr.min.css') }}" rel="stylesheet">
@stop
@section('content')

<section class="my_account_area common_top_margin">
    <div class="container custom_container">
        <div class="row">
            <div class="col-md-2">
                <div class="my_accout_menu common_left_menu">
                    @include('buyer.profile.menu')
                </div>
            </div>
            <div class="col-md-10">
                <div class="my_account_content">
                    <div class="myaccount_title"><h2>Change Avatar</h2></div>
                    <div class="my_info_area">
                        <div class="row">
                            <div class="col-md-12">
                                <form method="post" action="{{route('buyer_change_avatar')}}" enctype="multipart/form-data">
                                    @csrf
                                    <div class="form-group row"><label for="colFormImage"
                                                                        class="col-md-2 col-form-label col-form-label-md">Image</label>

                                        <div class="col-md-6">

                                            <div class="input-group">
                                                <div class="custom-file">
                                                    <input type="file" name="avatar" id="inputGroupFile01"
                                                            class="custom-file-input form-control form-control-md form-control-file"
                                                            style="height: auto !important;">
                                                    <label for="inputGroupFile01" class="custom-file-label">Choose image
                                                        file</label></div>
                                                <div class="input-group-append">
                                                    <button type="submit" class="btn btn-outline-secondary m-0">
                                                        Upload
                                                    </button>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-4">
                                            @if(isset($buyerAvatar->avatar))
                                            <img id="blah" align='middle'  src="{{asset(\App\Model\MetaBuyer::avatar_path().$buyerAvatar->avatar)}}"
                                                    class="float-right img-fluid" alt="your image" title=''/>
                                                @else
                                                <img id="blah" align='middle'  src="https://r3all.com/storage/user.png"
                                                        class="float-right img-fluid" alt="your image" title=''/>
                                            @endif
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@stop
@section('additionalJS')
    <script type="text/javascript" src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <script>
        $("#inputGroupFile01").change(function(event) {
            RecurFadeIn();
            readURL(this);
        });
        $("#inputGroupFile01").on('click',function(event){
            RecurFadeIn();
        });
        function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                var filename = $("#inputGroupFile01").val();
                filename = filename.substring(filename.lastIndexOf('\\')+1);
                reader.onload = function(e) {
                    debugger;
                    $('#blah').attr('src', e.target.result);
                    $('#blah').hide();
                    $('#blah').fadeIn(500);
                    $('.custom-file-label').text(filename);
                }
                reader.readAsDataURL(input.files[0]);
            }
            $(".alert").removeClass("loading").hide();
        }
        function RecurFadeIn(){
            console.log('ran');
            FadeInAlert("Wait for it...");
        }
        function FadeInAlert(text){
            $(".alert").show();
            $(".alert").text(text).addClass("loading");
        }
    </script>
@stop